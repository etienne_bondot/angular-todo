/*jshint latedef: nofunc*/

(function () {

    'use strict';

    angular.module('myApp')
        .service('TaskService', TaskService);

    TaskService.$inject = [
        '$http',
        '$q',
        '$log',
        'config'
    ];

    function TaskService($http, $q, $log, config) {
        var url = config.api.url;

        return {
            getTotalItem: function () {
                var d = $q.defer();
                function success(res) { d.resolve(res.data); }
                function error(err) {
                    if (err.data !== null && err.data.message !== null) {
                        $log.error(err.data.message);
                    }
                    d.reject();
                }
                $http.get(url + '/tasks/count').then(success, error);
                return d.promise;
            },

            get: function (taskId) {
                var d = $q.defer();
                function success(res) { d.resolve(res.data); }
                function error(err) {
                    if (err.data !== null && err.data.message !== null) {
                        $log.error(err.data.message);
                    }
                    d.reject();
                }
                $http.get(url + '/tasks/' + taskId).then(success, error);
                return d.promise;
            },

            getPage: function (itemsPerPage, currentPage) {
                var d = $q.defer();
                function success(res) { d.resolve(res.data); }
                function error(err) {
                    if (err.data !== null && err.data.message !== null) {
                        $log.error(err.data.message);
                    }
                    d.reject();
                }
                $http.get(url + '/tasks?currentPage=' + currentPage+ '&itemsPerPage='+ itemsPerPage)
                    .then(success, error);
                return d.promise;
            },

            delete: function (item) {
                var d = $q.defer();
                function success(res) { d.resolve(res.data); }
                function error(err) {
                    if (err.data !== null && err.data.message !== null) {
                        $log.error(err.data.message);
                    }
                    d.reject();
                }
                $http.delete(url + '/tasks/' + item._id).then(success, error);
                return d.promise;
            }
        };
    }

})();
