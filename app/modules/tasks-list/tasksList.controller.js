/*jshint latedef: nofunc*/

(function () {

    'use strict';

    angular.module('myApp')
        .controller('TasksListController', TasksListCtrl);

    TasksListCtrl.$inject = [
        '$scope',
        '$rootScope',
        '$state',
        'TaskService',
        '_'
    ];

    function TasksListCtrl($, $rootScope, $state, TaskService, _) {

        console.log($rootScope.taskListCurrentPage);

        $.totalItems = 0;
        $.itemsPerPage = 10;
        $.currentPage = 1;
        $.tasks = [];

        console.log('currentPage: ', $.currentPage);

        // ====================================================== Initialization
        TaskService.getTotalItem().then(function (res) {
            $.totalItems = res.data;
            $.currentPage = $rootScope.taskListCurrentPage || 1;
            TaskService.getPage($.itemsPerPage, $.currentPage - 1).then(function (res) {
                $.tasks = res.data;
            });
        });

        $.updateList = function () {
            TaskService.getPage($.itemsPerPage, $.currentPage - 1).then(function (res) {
                $.tasks = res.data;
            });
        };

        $.deleteTask = function (task, evt) {
            evt.stopPropagation();
            TaskService.delete(task).then(function () {
                _.remove($.tasks, function (t) {
                    return t._id === task._id;
                });

                // need to update the list pagination
                $.totalItems = $.totalItems - 1;
                $.updateList();
            });
        };

        $.gotoTaskDetail = function (task) {
            $rootScope.taskListCurrentPage = $.currentPage;
            $state.go('task-detail', { task_id: task._id });
        };

    }

})();
