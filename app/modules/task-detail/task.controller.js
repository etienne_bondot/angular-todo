/*jshint latedef: nofunc*/

(function () {

    'use strict';

    angular.module('myApp')
        .controller('TaskController', TaskCtrl);

    TaskCtrl.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        'TaskService'
    ];

    function TaskCtrl($, $state, $stateParams, TaskService) {

        // ====================================================== Initialization
        TaskService.get($stateParams.task_id).then(function (res) {
            $.task = res.data;
        });
        // =====================================================================

        $.delete = function () {
            TaskService.delete($.task).then(function () {
                $state.go('tasks-list');
            });
        };

    }

})();
