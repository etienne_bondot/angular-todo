/*jshint latedef: nofunc*/
/*jshint sub:true*/

(function () {

    'use strict';

    angular.module('myApp', [
        'ui.router',
        'ngAnimate',
        'ngResource',
        'ui.bootstrap'
    ])
    .constant('_', window._)
    .factory('config', appConfig)
    .factory('appInterceptor', appInterceptor)
    .config(appRouting);

    /**
     ** appConfig
     */

    appConfig.$inject = [];

    function appConfig() {
        var env = (function () {
            var url = window.document.URL;
            if (url.indexOf('http://127.0.0.1') === 0 || url.indexOf('http://localhost') === 0) {
                return 'development';
            } else {
                return 'production';
            }
        })();

        if (!env) {
            throw new Error('failed to detect application env');
        }
        return window.config[env];
    }

    /**
     ** appInterceptor
     */

    appInterceptor.$inject = [
        '$q',
        '$location'
    ];

    function appInterceptor($q, $location) {
        return {
            request: function (config) {
                return config;
            },
            requestError: function (rejection) {
                return $q.reject(rejection);
            },
            response: function (response) {
                return response;
            },
            responseError: function (response) {
                return $q.reject(response);
            }
        };
    }

    /**
     ** appRouting
     */

    appRouting.$inject = [
        '$stateProvider',
        '$httpProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

    function appRouting($stateProvider, $httpProvider, $urlRouterProvider, $locationProvider) {
        $httpProvider.interceptors.push('appInterceptor');

        // use the HTML5 History API
        $locationProvider.html5Mode(true);

        // For any unmatched url, redirect to /
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('tasks-list', {
                url: '/',
                templateUrl: './modules/tasks-list/tasks-list.view.html',
                controller: 'TasksListController'
            })
            .state('task-detail', {
                url: '/task/:task_id',
                templateUrl: './modules/task-detail/task-detail.view.html',
                controller: 'TaskController'
            });
    }

})();
