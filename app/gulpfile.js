var gulp = require('gulp'),
	shell = require('gulp-shell'),
	del = require('del'),
	less = require('gulp-less'),
	cleanCSS = require('gulp-clean-css'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	jshint = require('gulp-jshint'),
    browserSync = require('browser-sync'),
	historyApiFallback = require('connect-history-api-fallback'),
	package = require('./package.json');

gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: '.'
        }
    });
})

/**
 * Running Bower
 */
.task('bower', function () {
	shell('bower install');
})

/**
 * Cleaning dist/ folder
 */
.task('clean', function (cb) {
	del(['./dist/**'], cb);
})

/**
 * Less compilation
 */
.task('less', function () {
	return gulp.src(package.paths.less)
		.pipe(less())
		.pipe(concat(package.dest.style))
		.pipe(gulp.dest(package.dest.dist))
        .pipe(browserSync.reload({ stream:true }));
})
.task('less:min', function () {
	return gulp.src(package.paths.less)
		.pipe(less())
		.pipe(concat(package.dest.style))
		.pipe(cleanCSS())
		.pipe(gulp.dest(package.dest.dist))
        .pipe(browserSync.reload({ stream:true }));
})

/**
 * JSLint/JSHint validation
 */
.task('lint', function () {
	return gulp.src(package.paths.js)
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
})

/** JavaScript compilation */
.task('js', function () {
	return gulp.src([package.paths.app, package.paths.js])
		.pipe(concat(package.dest.app))
		.pipe(gulp.dest(package.dest.dist));
})
.task('js:min', function () {
	return gulp.src([package.paths.app, package.paths.js])
		.pipe(concat(package.dest.app))
		.pipe(uglify())
		.pipe(gulp.dest(package.dest.dist));
})

.task('watch', function () {
	gulp.watch(package.paths.less, ['less']);
	gulp.watch([package.paths.app, package.paths.js], ['lint', 'js']);
    gulp.watch(package.paths.html);
    gulp.watch(['./dist/**/*']).on('change', browserSync.reload);
})

/**
 * Compiling resources and serving application
 */
.task('serve', ['bower', 'clean', 'lint', 'less', 'js', 'server', 'watch'])
.task('serve:minified', ['bower', 'clean', 'lint', 'less:min', 'js:min'], function () {
	gulp.watch(package.paths.less, ['less:min']);
	gulp.watch([package.paths.app, package.paths.js], ['lint', 'js:min']);
});
