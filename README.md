++ API

Run a mongodb instance:
```
$> cd api
$> mkdir -p protected/db
$> mongod --dbpath protected/db/
```

Launch the server:
```
$> npm install
$> node server.js
```

++ FRONT

```
$> cd app
$> npm install
$> bower install
$> gulp serve
```
