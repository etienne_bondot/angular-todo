// ./app/models/user.js

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var TaskSchema = new Schema({
    name:       { type: String },
    createdAt:  { type: Date, private: true }
});

TaskSchema.pre('save', function(next) {
    if (!this.createdAt) this.createdAt = new Date();
    next();
});

module.exports = mongoose.model('Task', TaskSchema);
