// app/routes/index.js

var express = require('express');
var router = express.Router();

var helloworld = require('./helloworld.routes.js');
var tasks = require('./task.routes.js');

// helloworld routes
router.get('/', helloworld.index);

// users routes
router.post('/tasks', tasks.create);
router.get('/tasks',  tasks.getPage);
router.get('/tasks/count',  tasks.getTotalItem);
router.get('/tasks/:task_id', tasks.index);
router.delete('/tasks/:task_id', tasks.delete);

module.exports = router;
