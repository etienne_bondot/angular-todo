// app/routes/helloworld.routes.js

module.exports = {
    // GET /
    // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
    index: function(req, res) {
        res.json({ message: 'hooray! welcome to our api!' });
    }
};
