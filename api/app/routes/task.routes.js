// app/routes/user.routes.js

var Task = require('../models/task.model');
var config = require('../../config');

module.exports = {

    // POST /tasks
    // create a new task (accessed at POST http://localhost:8080/tasks)
    create: function(req, res, next) {
        var task = new Task(req.body);
        task.save(function(err) {
            if (err) return next(err);
            res.json({ success: true, data: {} });
        });
    },

    index: function (req, res, next) {
        Task.findById(req.params.task_id, function (err, task) {
            if (err) return next(err);
            res.json({ success: true, data: task });
        });
    },

    getPage: function (req, res, next) {
        var itemsPerPage = req.query.itemsPerPage,
            page = req.query.currentPage;

        Task.find().limit(itemsPerPage)
            .skip(itemsPerPage * page)
            .sort({
                name: 'asc'
            })
            .exec(function(err, tasks) {
                if (err) return next(err);
                res.json({ success: true, data: tasks });
            });
    },

    getTotalItem: function (req, res, next) {
        Task.find(function(err, tasks) {
            if (err) return next(err);
            res.json({ success: true, data: tasks.length });
        });
    },

    delete: function (req, res, next) {
        Task.findById(req.params.task_id).remove(function(err) {
            if (err) return next(err);
            res.json({ success: true, data: {} });
        });
    }

};
