// seed.js

var Chance          = require('chance');
var chance          = new Chance();
var Task            = require('./app/models/task.model');
var _               = require('lodash');

module.exports = function () {
    var tasks = [];

    // Drop the collection
    Task.remove({}, function (err) {
        if (err) console.log(err);
    });

    // Insert 30 new random users
    for (var i = 0; i < 30; i++) {
        tasks.push({
            name: chance.state({ full: true })
        });
    }
    _.forEach(tasks, function(task) {
        Task.create(task, function (err, task) {
            if (err) {
                console.log(err);
                return;
            }
        });
    });
};
