// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express         = require('express');        // call express
var app             = express();                 // define our app using express
var bodyParser      = require('body-parser');
var cors            = require('cors');
var morgan          = require('morgan');
var mongoose        = require('mongoose');
var config          = require('./config');

mongoose.connect(config.database);  // connect to our database
require('./seed')();                // seed the database

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// use morgan to log requests to the console
app.use(morgan('dev'));

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
app.use('/', require('./app/routes'));

// ERROR HANDLING
// =============================================================================

if (app.get('env') === 'development') {
    // error handling
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            success: false,
            message: err.message,
            error: err
        });
    });
}

// MIDDLEWARE
// =============================================================================

// app.use(function (req, res, next) {
//     next();
// });

// START THE SERVER
// =============================================================================

app.listen(port, function () {
    console.log('CORS-enable web server listening on port ' + port);
});
